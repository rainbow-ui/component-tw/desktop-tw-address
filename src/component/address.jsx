import config from 'config';
import { Component, UISmartPanelGrid, UIText, UIDialog, UISelect } from 'rainbowui-desktop-core';
import { Util } from 'rainbow-desktop-tools'
import PropTypes from 'prop-types';
import '../css/component.css';
import { CodeTableService } from 'rainbow-desktop-codetable';
import { ValidatorContext } from "rainbow-desktop-cache";

export default class TwAddress extends Component {
    constructor(props) {
        super(props)
        this.state = {
            required: this.props.required,
            cityOnChange: this.props.cityOnChange,
            townOnChange: this.props.townOnChange,
            postalOnChange: this.props.postalOnChange,
            detailOnChange: this.props.detailOnChange,
            postalOnBlur: this.props.postalOnBlur,
            postalOnFocus: this.props.postalOnFocus,
            detailonBlur: this.props.detailonBlur,
            detailOnFocus: this.props.detailOnFocus,
            postalOnKeyUp: this.props.postalOnKeyUp,
            searchInfo: {},
            dialogId: this.props.dialogId,
            preFixDetail: '',
            // AddressInfo:{
            //     AddressInfo:this.props.model.Address
            // }
        }
        if (!this.props.cityOnChange) {
            this.state.cityOnChange = (event) => {
                if (this.props.model) {
                    let city = this.props.model.City ? this.props.model.City.toString() : '';
                    let town = this.props.model.Town ? this.props.model.Town.toString() : '';
                    if (city == '') {
                        town = ''
                    }
                    if (this.props.model.PostCode != null || this.props.model.PostCode != "") {
                        this.props.model.PostCode = '';
                        this.props.model.Town = '';
                    }
                    let resultCity = '';
                    let cityCodeTableName = this.props.cityCodeTableName ? this.props.cityCodeTableName : 'TW_CITY';
                    CodeTableService.getCodeTableValue({ 'CodeTableName': cityCodeTableName, 'CodeTableKey': city }).then((data) => {
                        resultCity = data || '';
                        this.props.model.Address = '';
                        this.state.preFixDetail = resultCity;
                        if (this.state.preFixDetail == '') {
                            // if (this.state.AddressInfo && this.state.AddressInfo.AddressInfo) {
                            //     this.state.AddressInfo.AddressInfo = '';
                            // }
                        }
                        this.setState({});
                        if (this.props.onChange) {
                            this.props.onChange();
                        }
                    })
                }
            }
        }
        if (!this.props.townOnChange) {
            this.state.townOnChange = (event) => {
                if (this.props.model) {
                    let city = this.props.model.City ? this.props.model.City.toString() : '';
                    let town = this.props.model.Town ? this.props.model.Town.toString() : '';
                    let resultCity = '';
                    let resultTown = '';
                    let cityCodeTableName = this.props.cityCodeTableName ? this.props.cityCodeTableName : 'TW_CITY';
                    let townCodeTableName = this.props.townCodeTableName ? this.props.townCodeTableName : 'TW_AREA';
                    CodeTableService.getCodeTableValue({ 'CodeTableName': cityCodeTableName, 'CodeTableKey': city }).then((data) => {
                        resultCity = data || ''
                        CodeTableService.getCodeTableValue({ 'CodeTableName': townCodeTableName, 'CodeTableKey': town }).then((data) => {
                            resultTown = data || '';
                            // let addressInfo = this.props.model.Address||'';
                            // this.props.model.Address = addressInfo;
                            this.state.preFixDetail = resultCity + resultTown;
                            let twConditionKey = this.props.townConditionKey ? this.props.townConditionKey : 'CITY_CODE'
                            let twCondition = {};
                            twCondition[twConditionKey] = this.props.model.City;
                            CodeTableService.getCodeTable({ 'CodeTableName': townCodeTableName, 'ConditionMap': twCondition }).then((codeTable) => {
                                let townCodeTable = codeTable.codes;
                                townCodeTable.forEach(element => {
                                    if (element.id === this.props.model.Town) {
                                        this.props.model.PostCode = element && element.ConditionFields && element.ConditionFields[0] && element.ConditionFields[0].DataValue ? element.ConditionFields[0].DataValue : '';
                                    }
                                });
                                if (this.state.preFixDetail == '') {
                                    // this.props.model.AddressInfo = '';
                                    // if (this.state.AddressInfo && this.state.AddressInfo.AddressInfo) {
                                    //     this.state.AddressInfo.AddressInfo = '';
                                    // }
                                }
                                this.setState({});
                                if (this.props.model.PostCode) {
                                    ValidatorContext.validate("true", this.props.validationGroup, undefined, ValidatorContext.getValidators(this.postCode), undefined);
                                }
                                if (this.props.onChange) {
                                    this.props.onChange();
                                }
                            })
                        })
                    })
                }
            }
        }

        this.state.detailonBlur = (event) => {
            // let AddressInfo = this.props.model.AddressInfo || '';
            // if (this.state.AddressInfo && this.state.AddressInfo.AddressInfo) {
            //     this.props.model.Address = this.state.AddressInfo.AddressInfo; // this.state.preFixDetail+ AddressInfo;
            // } else {
            //     this.props.model.Address = AddressInfo;
            // }
            this.setState({});
            this.props.detailonBlur();
        }

        this.state.postalOnKeyUp = (event) => {
            if (this.props.model.PostCode && this.props.model.PostCode.length > 2) {
                let keyUpPostalCode = this.props.model.PostCode.substr(0, 3);
                let townCodeTableName = this.props.townCodeTableName ? this.props.townCodeTableName : 'TW_AREA';
                let cityCodeTableName = this.props.cityCodeTableName ? this.props.cityCodeTableName : 'TW_CITY';
                CodeTableService.getCodeTable({ 'CodeTableName': townCodeTableName }).then((codeTable) => {
                    let townCodeTable = codeTable.codes;
                    let townList = [];
                    townCodeTable.forEach(element => {
                        let itemPostCode = element && element.ConditionFields && element.ConditionFields[0] && element.ConditionFields[0].DataValue ? element.ConditionFields[0].DataValue : '';
                        if (itemPostCode.toString() === keyUpPostalCode) {
                            townList.push(element)
                            // this.props.model.Town = element.id;
                            // let twConditionKey = this.props.townConditionKey?this.props.townConditionKey:'CITY_CODE';
                            // this.props.model.City = element.ConditionFields[0][twConditionKey];
                        }
                    });
                    if (townList.length > 0) {
                        this.props.model.Town = townList[0].id;
                        let twConditionKey = this.props.townConditionKey ? this.props.townConditionKey : 'CITY_CODE';
                        this.props.model.City = townList[0].ConditionFields[0][twConditionKey];
                    }
                    let city = this.props.model.City ? this.props.model.City.toString() : '';
                    let town = this.props.model.Town ? this.props.model.Town.toString() : '';
                    let resultCity = '';
                    let resultTown = '';
                    CodeTableService.getCodeTableValue({ 'CodeTableName': cityCodeTableName, 'CodeTableKey': city }).then((data) => {
                        resultCity = data || ''
                        CodeTableService.getCodeTableValue({ 'CodeTableName': townCodeTableName, 'CodeTableKey': town }).then(async (data2) => {
                            resultTown = data2 || ''
                            // let addressInfo = this.props.model.Address||'';
                            // this.props.model.Address = addressInfo;
                            this.state.preFixDetail = resultCity + resultTown;
                            this.setState({});
                            // if(this.props.model.City && this.props.model.Town ){
                            //     await ValidatorContext.validate("true", this.props.validationGroup, undefined,null,undefined);                                
                            // }
                        })
                    })
                })
            }
            this.props.postalOnKeyUp();
        }

        this.state.postalOnBlur = (event) => {
            if (!this.props.model.PostCode || (this.props.model.PostCode && this.props.model.PostCode.length < 3)) {
                this.props.model.City = '';
                this.props.model.Town = '';
                this.state.preFixDetail = '';
                this.setState({});
            }
            this.props.postalOnBlur();
        }

        if (this.props.model && this.props.model.City && this.props.model.Town) {
            let townCodeTableName = this.props.townCodeTableName ? this.props.townCodeTableName : 'TW_AREA';
            let cityCodeTableName = this.props.cityCodeTableName ? this.props.cityCodeTableName : 'TW_CITY';
            let city = this.props.model.City ? this.props.model.City.toString() : '';
            let town = this.props.model.Town ? this.props.model.Town.toString() : '';
            let resultCity = '';
            let resultTown = '';
            CodeTableService.getCodeTableValue({ 'CodeTableName': cityCodeTableName, 'CodeTableKey': city }).then((data) => {
                resultCity = data || ''
                CodeTableService.getCodeTableValue({ 'CodeTableName': townCodeTableName, 'CodeTableKey': town }).then((data2) => {
                    resultTown = data2 || ''
                    this.state.preFixDetail = resultCity + resultTown;
                    this.setState({});
                })
            })
        }
        this.postCode = "address-postCode-" + this.generateId();
        this.cityId = "address-city-" + this.generateId();
        this.townId = "address-town-" + this.generateId();
    }

    generateId() {
        let index = new Date().getTime();
        return "rainbow-ui-" + (index++);
    }

    onDeleteIconClick() {
        this.props.model.Address = '';
        this.props.model.City = '';
        this.props.model.Town = '';
        this.state.preFixDetail = '';
        // if (this.state.AddressInfo && this.state.AddressInfo.AddressInfo) {
        //     this.state.AddressInfo.AddressInfo = '';
        // }
        // this.props.model.AddressInfo = ''
        this.setState({ preFixDetail: '' });
    }

    componentWillReceiveProps(nextProps) {
        this.state.required = nextProps.required;
        // this.state.AddressInfo.AddressInfo = this.props.model.Address
        if (event && event.target && event.target.className && event.target.className == 'input-remove') {
            return;
        }
        let city = nextProps.model.City ? nextProps.model.City.toString() : '';
        let town = nextProps.model.Town ? nextProps.model.Town.toString() : '';
        if (city == '') {
            town = ''
        }
        let resultCity = '';
        let resultTown = '';
        let cityCodeTableName = nextProps.cityCodeTableName ? nextProps.cityCodeTableName : 'TW_CITY';
        let townCodeTableName = nextProps.townCodeTableName ? nextProps.townCodeTableName : 'TW_AREA';
        CodeTableService.getCodeTableValue({ 'CodeTableName': cityCodeTableName, 'CodeTableKey': city }).then((data) => {
            resultCity = data || ''
            CodeTableService.getCodeTableValue({ 'CodeTableName': townCodeTableName, 'CodeTableKey': town }).then((data) => {
                resultTown = data || '';
                let addressInfo = nextProps.model.Address || '';
                this.state.preFixDetail = resultCity + resultTown;
                // this.props.model.AddressInfo = addressInfo.replace(resultCity + resultTown, '')
                // if (this.state.AddressInfo) {
                // this.state.AddressInfo.AddressInfo = addressInfo.replace(resultCity + resultTown, '')
                // }
                this.setState({});
            })
        })
    }

    render() {
        let searchIcon = Util.parseBool(this.props.searchIcon) ? true : false;
        let lableNoI18n = this.props.label ? false : true;
        let townCondition = {}
        if (this.props.townConditionKey) {
            townCondition[this.props.townConditionKey] = this.props.model.City
        }
        let layout = this.props.layout;
        return (
            <div>
                <UISmartPanelGrid className="desktop-tw-address" column="12">
                    {searchIcon ? <UIText id={this.postCode} enabled={this.props.enabled} label={this.props.label ? this.props.label : ''} noI18n={lableNoI18n} layout={layout} colspan="2" model={this.props.model} property="PostCode" required={this.state.required ? this.state.required : 'false'} validationGroup={this.props.validationGroup} onChange={this.state.postalOnChange.bind(this)} onBlur={this.state.postalOnBlur.bind(this)} onFocus={this.state.postalOnFocus.bind(this)} suffixIcon="rainbow Search" onSuffixIconClick={this.onSearchClick.bind(this)} onDeleteIconClick={this.onDeleteIconClick.bind(this)} maxLength="6" allowChars="0123456789" /> :
                        <UIText id={this.postCode} enabled={this.props.enabled} label={this.props.label ? this.props.label : ''} noI18n={lableNoI18n} layout={layout} colspan="2" model={this.props.model} property="PostCode" required={this.state.required ? this.state.required : 'false'} validationGroup={this.props.validationGroup} onChange={this.state.postalOnChange.bind(this)} onBlur={this.state.postalOnBlur.bind(this)} onFocus={this.state.postalOnFocus.bind(this)} onKeyUp={this.state.postalOnKeyUp} onDeleteIconClick={this.onDeleteIconClick.bind(this)} maxLength="6" allowChars="0123456789" />}

                    {this.props.cityCodeTableName ? <UISelect id={this.cityId} enabled={this.props.enabled} codeTableName={this.props.cityCodeTableName} layout={layout} colspan="2" widthAllocation="0,12" required={this.state.required ? this.state.required : 'false'} validationGroup={this.props.validationGroup} model={this.props.model} property="City" onChange={this.state.cityOnChange.bind(this)} /> :
                        this.props.cityCodeTable ? <UISelect id={this.cityId} enabled={this.props.enabled} codeTable={this.props.cityCodeTable} layout={layout} colspan="2" widthAllocation="0,12" required={this.state.required ? this.state.required : 'false'} validationGroup={this.props.validationGroup} model={this.props.model} property="City" onChange={this.state.cityOnChange.bind(this)} /> :
                            <UISelect id={this.cityId} enabled={this.props.enabled} layout={layout} colspan="2" widthAllocation="0,12" model={this.props.model} property="City" codeTableName="TW_CITY" required={this.state.required ? this.state.required : 'false'} validationGroup={this.props.validationGroup} onChange={this.state.cityOnChange.bind(this)} />
                    }
                    {this.props.townCodeTableName ? <UISelect id={this.townId} enabled={this.props.enabled} codeTableName={this.props.townCodeTableName} layout={layout} colspan="2" widthAllocation="0,12" required={this.state.required ? this.state.required : 'false'} validationGroup={this.props.validationGroup} model={this.props.model} property="Town" conditionMap={townCondition} onChange={this.state.townOnChange.bind(this)} /> :
                        this.props.townCodeTable ? <UISelect id={this.townId} enabled={this.props.enabled} codeTable={this.props.townCodeTable} layout={layout} colspan="2" widthAllocation="0,12" required={this.state.required ? this.state.required : 'false'} validationGroup={this.props.validationGroup} model={this.props.model} property="Town" conditionMap={townCondition} onChange={this.state.townOnChange.bind(this)} /> :
                            <UISelect id={this.townId} enabled={this.props.enabled} layout={layout} colspan="2" widthAllocation="0,12" model={this.props.model} property="Town" codeTableName="TW_AREA" required={this.state.required ? this.state.required : 'false'} validationGroup={this.props.validationGroup} conditionMap={{ 'CITY_CODE': this.props.model.City }} onChange={this.state.townOnChange.bind(this)} />
                    }

                    <UIText enabled={this.props.enabled} layout={layout} colspan="6" widthAllocation="0,12" prefixText={this.state.preFixDetail} model={this.props.model} property="Address" required={this.state.required ? this.state.required : 'false'} validationGroup={this.props.validationGroup} onChange={this.state.detailOnChange.bind(this)} onBlur={this.state.detailonBlur.bind(this)} onFocus={this.state.detailOnFocus.bind(this)} isShowValueTooltip={Util.parseBool(this.props.isShowAddressTooltip)} />
                </UISmartPanelGrid>
                {/* <UIDialog id={this.state.dialogId} className="searchDialog" width="95%" closeable="true">
                    <UISmartPanelGrid column='12' className="desktop-tw-address-dialog">
                        {this.props.cityCodeTableName?<UISelect codeTableName={this.props.cityCodeTableName} layout={layout} colspan="1" widthAllocation="0,12" model={this.state.searchInfo} property="city" onChange={this.state.cityOnChange.bind(this)}/>:
                        this.props.cityCodeTable?<UISelect codeTable={this.props.cityCodeTable} layout={layout} colspan="1" widthAllocation="0,12" model={this.state.searchInfo} property="city" onChange={this.state.cityOnChange.bind(this)}/>:
                        <UISelect layout={layout} colspan="1" widthAllocation="0,12" model={this.state.searchInfo} property="city" codeTableName="TW_CITY" onChange={this.state.cityOnChange.bind(this)}/>
                        }
                        {this.props.townCodeTableName?<UISelect codeTableName={this.props.townCodeTableName} layout={layout} colspan="1" widthAllocation="0,12" model={this.state.searchInfo} property="town" onChange={this.state.townOnChange.bind(this)}/>:
                        this.props.townCodeTable?<UISelect codeTable={this.props.townCodeTable} layout={layout} colspan="1" widthAllocation="0,12"model={this.state.searchInfo} property="town" onChange={this.state.townOnChange.bind(this)}/>:
                        <UISelect layout={layout} colspan="1" widthAllocation="0,12"model={this.state.searchInfo} property="town" codeTableName="TW_AREA" conditionMap={{"CITY_CODE":this.props.model.City}} onChange={this.state.townOnChange.bind(this)}/>
                        }
                        {this.props.streetCodeTableName?<UISelect codeTableName={this.props.streetCodeTableName} layout={layout} colspan="1" widthAllocation="0,12" model={this.state.searchInfo} property="street" onChange={this.state.townOnChange.bind(this)}/>:
                        this.props.streetCodeTable?<UISelect codeTable={this.props.streetCodeTable} layout={layout} colspan="1" widthAllocation="0,12"model={this.state.searchInfo} property="street" onChange={this.state.townOnChange.bind(this)}/>:
                        <UISelect layout={layout} colspan="1" widthAllocation="0,12"model={this.state.searchInfo} property="street" onChange={this.state.townOnChange.bind(this)}/>
                        }
                        <UISmartPanelGrid className="desktop-tw-address-dialog-detail" column='12' colspan="9">
                            <UIText layout={layout} colspan="1" widthAllocation="0,12" model={this.state.searchInfo} property='alley' />
                            <UIText label={r18n.alley} layout={layout} colspan="2" widthAllocation="4,8" model={this.state.searchInfo} property='lane' />
                            <UIText label={r18n.lane} layout={layout} colspan="2" widthAllocation="4,8" model={this.state.searchInfo} property='streetNo' />
                            <UIText label={r18n.streetNo} layout={layout} colspan="2" widthAllocation="4,8" model={this.state.searchInfo} property='floor1' />
                            <UIText layout={layout} colspan="1" widthAllocation="0,12" model={this.state.searchInfo} property='floor2' />
                            <UIText label={r18n.floor} layout={layout} colspan="2" widthAllocation="4,8" model={this.state.searchInfo} property='room1' />
                            <UIText layout={layout} colspan="1" widthAllocation="0,12" model={this.state.searchInfo} property='room2' />
                            <UILabel label={r18n.room} size="1x" colspan="1"/>
                        </UISmartPanelGrid>
                    </UISmartPanelGrid>
                    <UIBox direction="center">
                        <UIButton value={r18n.sure} styleClass="primary" onClick={this.Search.bind(this)} causeValidation='true' validationGroup="newClaimBasic" />
                    </UIBox>
                </UIDialog> */}
            </div>
        )
    }

    onSearchClick() {
        UIDialog.show(this.state.dialogId)
    }

    Search() {
        UIDialog.hide(this.state.dialogId)
    }

}

TwAddress.propTypes = $.extend({}, Component.propTypes, {
    label: PropTypes.string,
    required: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    searchIcon: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    isShowAddressTooltip: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    cityOnChange: PropTypes.func,
    townOnChange: PropTypes.func,
    postalOnChange: PropTypes.func,
    detailOnChange: PropTypes.func,
    postalOnBlur: PropTypes.func,
    postalOnFocus: PropTypes.func,
    detailonBlur: PropTypes.func,
    detailOnFocus: PropTypes.func,
    postalOnKeyUp: PropTypes.func,
    dialogId: PropTypes.string,
    layout: PropTypes.oneOf(['horizontal', 'vertical'])
});


TwAddress.defaultProps = $.extend({}, Component.defaultProps, {
    required: false,
    searchIcon: false,
    dialogId: 'searchDialog',
    layout: config.DEFAULT_INPUT_LAYOUT,
    isShowAddressTooltip: true,
    postalOnChange: () => {
    },
    detailOnChange: () => {
    },
    postalOnBlur: () => {
    },
    postalOnFocus: () => {
    },
    detailonBlur: () => {
    },
    detailOnFocus: () => {
    },
    postalOnKeyUp: () => {
    }
});
